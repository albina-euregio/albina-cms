# ALBINA CMS


## System requirements
- A Linux/Unix system.
- An Apache HTTP Server version 2.4 or greater.
- A PostgreSQL Database version 9.4 or greater.
- Check the Drupal 8 requirements at https://www.drupal.org/docs/8/system-requirements
- Install the latest version of https://getcomposer.org/


## Installation
You will usually not need to reinstall the CMS, since it is already installed
on the server. A common task, however, is Updating the installation due to
security updates of Drupal - check the section "updating Drupal" below!!!

- Copy the contents of this project to a location on your server.
- Run `composer install` in the base directory of your server.
- Install the initial database content: Use an existing database dump - this is 
  not part of the repository, since login credentials will be exposed otherwise.
- Install the initial uploaded content: Use a backup of the existing uploaded 
  content - this is not part of the repository since its content changes frequently.
- Update the Database settings in `web/sites/default/settings.php` to match your database connection.
- Update the CORS settings in `web/sites/default/services.yml`
- Make the `web/sites/default/files` directory writable to Apache:
```
$ sudo chcon -R -t httpd_sys_content_rw_t /var/www/html/public/web/sites/default/files
```

## Updating Drupal
To check the health of your installation, visit this link
https://cms.avalanche.report/admin/reports/updates

Please also check the Drupal security announce channel at https://www.drupal.org/security or, even better, subscribe to the twitter or rss feed. Moderately and highly critical Drupal security vulnerabilities are also pubished at https://www.heise.de/security/

There are two types of updates:
- Updates of Drupal core
- Updates of Modules

Updates of core are more work and it is highly recommended to do them on a clone of the live system and to make the clone the new live version once the update has finished.

### Updating the core
1. Copy your stable version to the dev version:
```
$ sudo mkdir /var/www/html/dev
$ sudo rsync -av /var/www/html/stable/ /var/www/html/dev/
$ sudo chcon -R -t httpd_sys_content_rw_t /var/www/html/dev/web/sites/default/files
```

2. Copy the contents of the database:
```
$ sudo su - postgres
$ pg_dump albina_cms > /tmp/cms.sql
$ createdb -E UTF8 -O albina albina_cms_old
$ psql albina_cms_old < /tmp/cms.sql
$ exit
$ vim /var/www/html/stable/web/sites/default/settings.php # change the database settings to 'albina_cms_old'
```

3. Run the updates:
```
$ cd /var/www/html/dev/
$ composer update drupal/core webflo/drupal-core-require-dev --with-dependencies
$ cd web
$ ../bin/drush updb
$ ../bin/drush entup
$ ../bin/drush cr
$ vim .htaccess # Change the RewriteBase setting to "/dev"
```
If the allowed memory limit is exceeded, use this composer update command
```
$ php -d memory_limit=-1 `which composer` update drupal/core webflo/drupal-core-require-dev --with-dependencies
```

If composer complains about modified files, just override these files. The modification
is due to the patches applied to Drupal. Composer will re-apply these patches 
after updating the packages.

4. Test the updated version:

- Go to https://cms.avalanche.report/dev/ and try to log in. Check the status messages at https://cms.avalanche.report/dev/admin/reports/status
- Try to make some API requests:
    * https://cms.avalanche.report/dev/de/api/menuLinks?fields[menuLinks]=internalId%2Ctitle%2Clink%2CmenuInternalId%2CparentIs%2CisExternal&sort=weight
    * https://cms.avalanche.report/dev/router/translate-path?_format=json&path=/weather/measurements
    * https://cms.avalanche.report/dev/de/api/pages/baed444c-66f7-4728-8c56-9b3ba4abfd7f

5. Make the dev version the new live version
```
$ cd /var/www/html/dev/web
$ vim .htaccess # Change RewriteBase setting to "/"
$ ../bin/drush cr # Rebuild the cache
$ cd /var/www/html/
$ sudo mv stable old && mv dev stable
```
- Check the health of your system.
- You can now remove the `old` directory and the old database.


### Updating modules

Go to the `web` folder of your project - e.g. `/var/www/html/stable/web/` and run the following commands:
```
$ ../bin/drush up    # this will update your modules
$ ../bin/drush updb  # this will run database updates - if any
$ ../bin/drush entup # this will run entity updates - if any
$ ../bin/drush cr    # rebuild the cache
```
